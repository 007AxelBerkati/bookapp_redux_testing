import axios from 'axios';
import { GET_BOOKS_API, LOGIN_USER, REGISTER_USER } from '../../config';
import { showError, showSuccess } from '../../utils';
import { AverageRatingSort } from '../../utils/helperFunction';
import {
  GET_BOOKS_ID,
  GET_BOOKS_POPULAR,
  GET_BOOKS_RECOMMENDED,
  LOGIN, LOGIN_LOADING, LOGOUT,
  REGISTER, REGISTER_LOADING, SET_LOADING, SET_ONLINE, SET_REFRESHING,
} from '../types';

export const setLoading = (value) => ({
  type: SET_LOADING,
  payload: value,
});

export const register = () => ({
  type: REGISTER,
});

export const registerLoading = (value) => ({
  type: REGISTER_LOADING,
  payload: value,
});

export const login = (token, name) => ({
  type: LOGIN,
  payload: token,
  name,
});

export const loginLoading = (value) => ({
  type: LOGIN_LOADING,
  payload: value,
});

export const saveBookPopular = (data) => ({
  type: GET_BOOKS_POPULAR,
  payload: data,
});

export const saveBookRecommended = (data) => ({
  type: GET_BOOKS_RECOMMENDED,
  payload: data,
});

export const saveBookId = (data) => ({
  type: GET_BOOKS_ID,
  payload: data,
});

export const online = (value) => ({
  type: SET_ONLINE,
  payload: value,
});

export const refresh = (value) => ({
  type: SET_REFRESHING,
  payload: value,
});

export const logout = () => ({
  type: LOGOUT,
});

// LOGIN
export const loginUser = (email, password, navigation) => async (dispatch) => {
  dispatch(loginLoading(true));
  console.log(email, password);
  try {
    await axios.post(LOGIN_USER, { email, password })
      .then((response) => {
        if (response.data.tokens.access.token) {
          dispatch(login(response.data.tokens.access.token, response.data.user.name));
          navigation.replace('HomeTabScreen');
          showSuccess('Login Sukses');
        }
      });
  } catch (error) {
    showError(error.message);
    dispatch(loginLoading(false));
  }
};

// GET_BOOKS_API_RECOMMENDED
export const getDataBooksRecommended = (token, limit, navigation) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(GET_BOOKS_API, { headers: { Authorization: `Bearer ${token}` }, params: { limit } })
      .then((response) => {
        const sortData = response.data.results.sort(AverageRatingSort);
        dispatch(saveBookRecommended(sortData));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
    dispatch(logout());
    navigation.replace('SplashScreen');
  }
};

// GET_BOOKS_API_POPULAR
export const getDataBooksPopular = (token) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(GET_BOOKS_API, { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => {
        const sortData = response.data.results.sort(AverageRatingSort);
        dispatch(saveBookPopular(sortData));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
    dispatch(setLoading(false));
  }
};

// GET_BOOKS_API_BY_ID
export const getDataBooksId = (token, id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(`${GET_BOOKS_API}/${id}`, { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => {
        dispatch(saveBookId(response.data));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
    dispatch(setLoading(false));
  }
};

// REGISTER
export const signupUser = (name, email, password, navigation) => async (dispatch) => {
  dispatch(registerLoading(true));
  try {
    await axios.post(REGISTER_USER, { email, password, name })
      .then(() => {
        dispatch(register());
        showSuccess('Register Berhasil');
        navigation.replace('SuccessRegisterScreen');
      });
  } catch (error) {
    dispatch(registerLoading(false));
    showError(error.message);
  }
};
