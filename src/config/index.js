export const API_BASE_URL = 'http://code.aldipee.com/api/v1';
export const getApiUrl = endpoint => API_BASE_URL + endpoint;

// AUTH
export const LOGIN_USER = getApiUrl('/auth/login');
export const REGISTER_USER = getApiUrl('/auth/register');

// GET_API
export const GET_BOOKS_API = getApiUrl('/books');
